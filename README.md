# html-pruning

A Go package that provides a function to truncate HTML code. 

## How to Use

Import the package in your Go program.

```
import (
    "gitlab.com/lhmzhou/html-pruning"
)
```

Call the function`htmlPrune`. Pass in the `byte` slice, the `maxlen`, and the `string` that should be appended to the truncated HTML. 

```
func htmlPrune(buf []byte, maxlen int, ellipsis string) ([]byte, error)
```


