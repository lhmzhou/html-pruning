package main

import "testing"

func TestHtmlPrune(t *testing.T) {
  cases := []struct {
      in string
      limit int
      ellipsis string
      want string
  }{
    {
      "",
      0,
      "",
      "",
    },
    {
      "",
      5,
      "",
      "",
    },
    {
      "123",
      0,
      "",
      "",
    },
    {
      "123",
      2,
      "",
      "12",
    },
    {
      "123",
      3,
      "",
      "123",
    },
    {
      "1234",
      3,
      "",
      "123",
    },
    {
      "<b>123</b>",
      5,
      "",
      "<b>123</b>",
    },
    {
      "<b>12345</b>",
      5,
      "",
      "<b>12345</b>",
    },
    {
      "<b>1234567</b>",
      5,
      "",
      "<b>12345</b>",
    },
    {
      "<b>Monty Python</b>",
      5,
      "",
      "<b>Monty</b>",
    },
    {
      "<img />",
      5,
      "",
      "<img />",
    },
    {
      "<img>",
      5,
      "",
      "<img>",
    },
    {
      "<h1><u>1234567</u></h1>",
      5,
      "...",
      "<h1><u>12345...</u></h1>",
    },
    {
      "<h1><u>1234 &copy; 1234</u></h1>",
      5,
      "",
      "<h1><u>1234 &copy;</u></h1>",
    },
    {
      "<h1><u>&copy;</u></h1>",
      1,
      "",
      "<h1><u>&copy;</u></h1>",
    },
    {
      "<h1><u>1234 &copy; 1234</u></h1>",
      6,
      "",
      "<h1><u>1234 &copy; 1</u></h1>",
    },
  }

  for _, c := range cases {
    out, err := htmlPrune([]byte(c.in), c.limit, c.ellipsis)
    got := string(out)
    if err != nil {
      t.Errorf("Got error calling htmlPrune(%q, 5, \"\"). Wanted: %q. Error:", c.in, c.want, err.Error())
    }
    if got != c.want {
      t.Errorf("htmlPrune(%q, %d, %q) == %q, want %q", c.in, c.limit, c.ellipsis, got, c.want)
    }
  }
}
